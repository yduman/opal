/* BSD 2-Clause License - see OPAL/LICENSE for details. */
package org.opalj.tac

//import java.io.{File, PrintWriter}
import java.net.URL

import org.opalj.br.{ClassFile, Method}
import org.opalj.br.analyses.Project

import scala.collection.mutable
import util.control.Breaks._
import org.opalj.bytecode.JRELibraryFolder

object UselessBuilderAnalysis {
  def main(args: Array[String]): Unit = {
    //val jar = "C:\\Users\\yduman\\IdeaProjects\\java-playground\\out\\artifacts\\java_playground_jar\\java-playground.jar"
    //val p: Project[URL] = Project(new java.io.File(jar))
    val p: Project[URL] = Project(JRELibraryFolder)

    val tacProvider = p.get(DefaultTACAIKey)
    val results = mutable.Set[String]()
    val resultsInterfaceTypes = mutable.Set[String]()

    // collect all builder classes and their interface types
    p.parForeachClassFile() {
      cf => {
        if (!cf.isAbstract) {
          var methodsReturningThisType = 0.0
          val methodCount = cf.methodsWithBody.filter(m => !m.isConstructor && !m.isSynthetic && !m.isStaticInitializer).size

          for {
            m <- cf.methods
            if m.body.isDefined && !m.isConstructor && !m.isSynthetic && !m.isStaticInitializer
            tac = tacProvider(m)
            if tac.cfg.normalReturnNode.predecessors.iterator.map(bb => tac.stmts(bb.asBasicBlock.endPC)).length == 1
            returnStmts = tac.cfg.normalReturnNode.predecessors.iterator.map(bb => tac.stmts(bb.asBasicBlock.endPC))
            ReturnValue(_, UVar(_, defSites)) <- returnStmts
            defSite <- defSites
          } {
            if (defSite == -1)
              methodsReturningThisType += 1.0
          }

          val isAboveThreshold = methodsReturningThisType / methodCount >= 0.3
          if (methodCount >= 4 && isAboveThreshold) {
            addToResults(results, resultsInterfaceTypes, cf)
          } else if (cf.thisType.toJava.contains("Builder") && methodCount >= 2 && isAboveThreshold) {
            addToResults(results, resultsInterfaceTypes, cf)
          }
        }
      }
    }

    // String -> (String, String, String)
    // caller side -> (methodName, builderMethodType, builderMethodName)
    val mm = new mutable.HashMap[String, mutable.Set[(String, String, String)]]
      with mutable.MultiMap[String, (String, String, String)]

    // Analyze all methods using the builder- and interface-types in our result sets
    p.parForeachMethod() {
      m => {
        if (m.body.isDefined && !m.isConstructor && !m.isSynthetic && !m.isStaticInitializer) {
          val tac = tacProvider(m)
          for {
            Assignment(pc, DVar(value, useSites), expr) <- tac.stmts
            if value.isReferenceValue && value.asReferenceValue.valueType.isDefined
            builderType = value.asReferenceValue.valueType.head.toJava
            useSite <- useSites
            if hasTypeInResults(results, resultsInterfaceTypes, builderType)
          } {
            expr match {
              case StaticFunctionCall(_, _, _, sfcName, sfcDescr, _) =>
                breakable {
                  for (stmt <- tac.stmts.drop(useSite)) {
                    stmt match {
                      case Assignment(_, DVar(_, _), VirtualFunctionCall(_, _, _, vfcName, vfcDescr, UVar(vfcVal, _), _)) =>
                        val valueType = vfcVal.asReferenceValue.valueType.head.toJava
                        if (hasBuilderType(resultsInterfaceTypes, builderType, valueType)) {
                          val builderRoot = s"PC($pc): ${sfcDescr.toJava(sfcName)}"
                          val builderMethodType = vfcDescr.returnType.toJava
                          val mmKey = s"${getPath(m)} --- Call @ $builderRoot"
                          val mmVal = (m.name, builderMethodType, vfcName)
                          mm.addBinding(mmKey, mmVal)
                        }
                      case VirtualMethodCall(_, _, _, _, _, _, _) => break
                      case _ =>
                    }
                  }
                }
              case GetField(gfPC, _, gfName, declaredFieldType, _) =>
                val fieldType = declaredFieldType.toJava
                if (hasTypeInResults(results, resultsInterfaceTypes, fieldType)) {
                  breakable {
                    for (stmt <- tac.stmts.drop(useSite)) {
                      stmt match {
                        case Assignment(_, DVar(_, _), VirtualFunctionCall(_, _, _, vfcName, vfcDescr, UVar(vfcVal, _), _)) =>
                          val valueType = vfcVal.asReferenceValue.valueType.head.toJava
                          if (hasBuilderType(resultsInterfaceTypes, builderType, valueType)) {
                            val builderRoot = s"PC($gfPC) $gfName"
                            val builderMethodType = vfcDescr.returnType.toJava
                            val mmKey = s"${getPath(m)} --- Call @ $builderRoot"
                            val mmVal = (m.name, builderMethodType, vfcName)
                            mm.addBinding(mmKey, mmVal)
                          }
                        case VirtualMethodCall(_, _, _, _, _, _, _) => break
                        case _ =>
                      }
                    }
                  }
                }
              case _ =>
            }
          }
          //writeToFile(mm)
        }
      }
    }
    findUselessBuilders(mm, results, resultsInterfaceTypes)
  }

  private def hasBuilderType(resultsInterfaceTypes: mutable.Set[String], builderType: String, valueType: String) = {
    valueType.equals(builderType) || resultsInterfaceTypes.contains(valueType)
  }

  private def hasTypeInResults(results: mutable.Set[String], resultsInterfaceTypes: mutable.Set[String], fieldType: String) = {
    results.contains(fieldType) || resultsInterfaceTypes.contains(fieldType)
  }

  private def findUselessBuilders(mm: mutable.HashMap[String, mutable.Set[(String, String, String)]] with mutable.MultiMap[String, (String, String, String)], results: mutable.Set[String], resultsInterfaceTypes: mutable.Set[String]): Unit = {
    val uselessBuilders = mutable.Set[String]()

    for ((k, v) <- mm) {
      var hits = 0
      v.foreach(triple => {
        if (!results.contains(triple._2) && !resultsInterfaceTypes.contains(triple._2))
          hits += 1
      })
      if (hits == 0)
        uselessBuilders += k
    }

    println(s"[FINISHED] ${uselessBuilders.size} result(s)")
    uselessBuilders.toSeq.sorted.foreach(println)
  }

  private def getPath(m: Method) = {
    s"${m.classFile.thisType.toJava}.${m.name}"
  }

  /*
  private def writeToFile(mm: mutable.HashMap[String, mutable.Set[(String, String, String)]] with mutable.MultiMap[String, (String, String, String)]): Unit = {
    val pw = new PrintWriter(new File("C:\\Users\\yduman\\Desktop\\mm.txt"))
    for ((k, v) <- mm) {
      pw.write(s"\n\n$k\n")
      v.foreach(triple => pw.write(s"    (${triple._1}, ${triple._2}, ${triple._3})\n"))
    }
    pw.close()
  }
  */

  private def addToResults(results: mutable.Set[String], resultsInterfaceTypes: mutable.Set[String], cf: ClassFile): Unit = {
    results += cf.thisType.toJava
    if (!cf.interfaceTypes.isEmpty) {
      cf.interfaceTypes.foreach(iType => resultsInterfaceTypes += iType.toJava)
    }
  }
}


