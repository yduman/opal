/* BSD 2-Clause License - see OPAL/LICENSE for details. */
package org.opalj.tac

//import java.io.{File, PrintWriter}
import java.net.URL

import org.opalj.br.ClassFile
import org.opalj.br.analyses.Project

import scala.collection.mutable
//import org.opalj.bytecode.JRELibraryFolder

object BuilderClassification {

  def main(args: Array[String]): Unit = {
    //val jar = "/home/prox/Desktop/backend-10.64.0.0-SNAPSHOT.jar"
    val jar = "C:\\Users\\yduman\\IdeaProjects\\java-playground\\out\\artifacts\\java_playground_jar\\java-playground.jar"
    val p: Project[URL] = Project(new java.io.File(jar))
    //val p: Project[URL] = Project(JRELibraryFolder)
    val tacProvider = p.get(DefaultTACAIKey)
    val results = mutable.Set[String]()
    val resultsInterfaceTypes = mutable.Set[String]()

    p.parForeachClassFile() {
      cf => {
        if (!cf.isAbstract) {
          var methodsReturningThisType = 0.0
          val methodCount = cf.methodsWithBody.filter(m => !m.isConstructor && !m.isSynthetic && !m.isStaticInitializer).size

          for {
            m <- cf.methods
            if m.body.isDefined && !m.isConstructor && !m.isSynthetic && !m.isStaticInitializer
            tac = tacProvider(m)
            if tac.cfg.normalReturnNode.predecessors.iterator.map(bb => tac.stmts(bb.asBasicBlock.endPC)).length == 1
            returnStmts = tac.cfg.normalReturnNode.predecessors.iterator.map(bb => tac.stmts(bb.asBasicBlock.endPC))
            ReturnValue(_, UVar(_, defSites)) <- returnStmts
            defSite <- defSites
          } {
            if (defSite == -1)
              methodsReturningThisType += 1.0
          }

          val isAboveThreshold = methodsReturningThisType / methodCount >= 0.3
          if (methodCount >= 4 && isAboveThreshold) {
            addToResults(results, resultsInterfaceTypes, cf)
          } else if (cf.thisType.toJava.contains("Builder") && methodCount >= 2 && isAboveThreshold) {
            addToResults(results, resultsInterfaceTypes, cf)
          }
        }
      }
    }
    // writeToFile(results, "/home/prox/Desktop/BuilderClassifierOutputWithNewResults.txt")
    printResults(results)
  }

  /*
  private def writeToFile(results: mutable.Set[String], pathname: String): Unit = {
    val pw = new PrintWriter(new File(pathname))
    for (v <- results.toSeq.sorted) {
      pw.write(s"$v\n")
    }
    pw.close()
  }
  */
  private def printResults(results: mutable.Set[String]): Unit = {
    println(s"[FINISHED] ${results.size} results")
    results.toSeq.sorted.foreach(x => println(s"==> $x"))
  }

  private def addToResults(results: mutable.Set[String], resultsInterfaceTypes: mutable.Set[String], cf: ClassFile): Unit = {
    println(s"type cf :: ${cf.thisType.toJava}")
    results += cf.thisType.toJava
    if (!cf.interfaceTypes.isEmpty) {
      cf.interfaceTypes.foreach(iType => resultsInterfaceTypes += iType.toJava)
    }
  }
}
