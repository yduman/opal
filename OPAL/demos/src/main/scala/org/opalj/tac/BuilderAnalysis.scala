/* BSD 2-Clause License - see OPAL/LICENSE for details. */
package org.opalj.tac

import java.io._
import java.net.URL
import org.opalj.br.analyses.Project
import scala.collection.mutable
import collection.mutable.{ HashMap, MultiMap, Set }
import util.control.Breaks._
// import org.opalj.bytecode.JRELibraryFolder

object BuilderAnalysis {
  def main(args: Array[String]): Unit = {
    // val jar = "C:\\Projects\\JavaPlayground\\out\\artifacts\\JavaPlayground\\JavaPlayground.jar"
    // val jar = "C:\\Users\\YadullahD\\Desktop\\backend-10.64.0.0-SNAPSHOT.jar"
    // val jar = "C:\\Users\\YadullahD\\Desktop\\PlanungsgegenstandEventStoreRepository.class"
    val jar = "C:\\Users\\yduman\\IdeaProjects\\java-playground\\out\\artifacts\\java_playground_jar\\java-playground.jar"
    val p: Project[URL] = Project(new java.io.File(jar))
    // val p: Project[URL] = Project(JRELibraryFolder)
    val tacProvider = p.get(DefaultTACAIKey)

    val mm = new HashMap[String, Set[mutable.SortedMap[Int, String]]] with MultiMap[String, mutable.SortedMap[Int, String]]

    p.parForeachMethod() {
      method =>
        try {
          val tac = tacProvider(method)
          // printTACforEachMethod(method, tac)
          for {
            Assignment(pcRoot, DVar(value, useSites), StaticFunctionCall(_, _, _, n, descr, _)) <- tac.stmts
            if !n.contains("access")
            useSite <- useSites
          } {
            val path = s"${method.classFile.thisType.toJava}.${method.name}"
            breakable {
              for (stmt <- tac.stmts.drop(useSite)) {
                stmt match {
                  case Assignment(_, DVar(_, _), VirtualFunctionCall(pc, _, _, name, descriptor, UVar(v, _), _)) =>
                    val dVarValue = value.asReferenceValue.valueType.head.toJava
                    val uVarValue = v.asReferenceValue.valueType.head.toJava

                    if (dVarValue.equals(uVarValue)) {
                      val builderRootString = s"PC($pcRoot): ${descr.toJava(n)}"
                      val builderMethodString = s"${descriptor.toJava(name)}"
                      val mmKey = s"$path --- Call @ $builderRootString"
                      val mmValue = mutable.SortedMap(pc.hashCode() -> builderMethodString)
                      mm.addBinding(mmKey, mmValue)
                    }

                  case Assignment(_, DVar(_, _), _) =>
                  case _ => break
                }
              }
            }
          }
          writeResultsToFile(mm)
        } catch {
          case _: Throwable =>
        }
    }
  }

  /*
  private def printTACforEachMethod(method: Method, tac: TACode[TACMethodParameter, DUVar[ValueInformation]]) = {
    println("---------------------------")
    println(method.name)
    tac.stmts.foreach(println)
    println("---------------------------")
  }
  */

  private def writeResultsToFile(mm: mutable.HashMap[String, mutable.Set[mutable.SortedMap[Int, String]]] with mutable.MultiMap[String, mutable.SortedMap[Int, String]]): Unit = {
    val pw = new PrintWriter(new File("C:\\Users\\yduman\\Desktop\\out.txt" ))
    for ((k, v) <- mm) {
      pw.write(s"\n\nClass: $k\n")
      v.foreach(map => map.toSeq.foreach(x => pw.write(s"    PC: ${x._1} Method: ${x._2}\n")))
    }
    pw.close()
  }
}